package com.yella.testng.tests;



import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class CreatLead extends Annotation{
	
	
	@BeforeTest(groups="smoke")
	public void setData() {
		testcaseName= "TC001_CreatLead";
		testcaseDec = "Create a new Lead in leaftaps";
		author      = "Gayatri";
		category    = "Smoke";
		 excelFileName="createlead";
	}
	/*@DataProvider(name="CreateData")
    public Object[][] fetchData()
	{
		//Object[][] data=new Object[2][3];
		/*data[0][0]="TestLeaf";
		data[0][1]="gayatri";
		data[0][2]="S";
		data[1][0]="TestLeaf";
		data[1][1]="sri uma";
		data[1][2]="S";
		
		return new Object[][] {{"TestLeaf"},{"gayatri"},{"S"}};
		
		}*/
	
	
	@Test(groups="smoke",dataProvider="CreateData")
	public void createLead(String data) {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), data);
		clearAndType(locateElement("id", "createLeadForm_firstName"), data);
		clearAndType(locateElement("id", "createLeadForm_lastName"), data);
		click(locateElement("name", "submitButton")); 
	}
	

}
