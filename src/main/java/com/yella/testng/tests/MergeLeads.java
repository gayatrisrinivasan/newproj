package com.yella.testng.tests;

import org.testng.annotations.Test;

public class MergeLeads extends Annotation {
	@Test(enabled=false)
	public void mergeLead() throws InterruptedException
	{
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("link", "Merge Leads")); 
		click(locateElement("xpath","//img[@alt='Lookup']"));
		switchToWindow("Find Leads");
		clearAndType(locateElement("xpath", "//input[@name='firstName']"), "gayathri");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(5000);
	}

}
